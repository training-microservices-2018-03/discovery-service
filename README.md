# Aplikasi Discovery Service #

Cara mendeploy aplikasi ke Heroku

1. Buat dulu aplikasi di Dashboard Heroku. Kita akan mendapatkan nama aplikasi dan URL git untuk deployment

2. Daftarkan url git sebagai remote

        git remote add heroku-eu https://git.heroku.com/tm2018-discovery-service-eu.git

3. Deploy dengan cara push

        git push heroku-eu master

4. Pantau log aplikasi

        heroku logs --tail -a tm2018-discovery-service-eu